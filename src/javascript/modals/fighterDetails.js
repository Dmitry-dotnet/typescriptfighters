import { createElement } from "../helpers/domHelper";
import { showModal } from "./modal";
export function showFighterDetailsModal(fighter) {
    const title = "Fighter info";
    const bodyElement = createFighterDetails(fighter);
    showModal({ title, bodyElement });
}
function createFighterDetails(fighter) {
    const { name } = fighter;
    const fighterDetails = createElement({ tagName: "div", className: "modal-body" });
    const nameElement = createElement({ tagName: "p", className: "fighter-name" });
    const healthElement = createElement({ tagName: "p", className: "fighter-health" });
    const attackElement = createElement({ tagName: "p", className: "fighter-attack" });
    const defenseElement = createElement({ tagName: "p", className: "fighter-defense" });
    const imgElement = createElement({ tagName: "span", className: "fighter-image" });
    // show fighter name, attack, defense, health, image
    nameElement.innerHTML = "<h2>" + name + "</h2>";
    healthElement.innerHTML = "<h3 style='color : red'> HEALTH -  " + fighter.health + "</h2>";
    attackElement.innerHTML = "<h3 style='color : black'> ATTACK -  " + fighter.attack + "</h2>";
    defenseElement.innerHTML = "<h3 style='color : blue'> DEFENSE -  " + fighter.defense + "</h2>";
    imgElement.innerHTML = "<img src='" + fighter.source + "' style='width: 65%'>";
    fighterDetails.append(nameElement, healthElement, attackElement, defenseElement, imgElement);
    return fighterDetails;
}
