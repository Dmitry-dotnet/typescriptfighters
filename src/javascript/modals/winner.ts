import { createElement } from "../helpers/domHelper";
import { IFighterDetails } from "../Interfaces/IFighterDetails";
import { showModal } from "./modal";

export function showWinnerModal(fighter: IFighterDetails) {
  const title = "WINNER";
  const bodyElement =  createBody(fighter);
  showModal({title, bodyElement});
  // show winner name and image
}


function createBody(fighter: IFighterDetails) : HTMLElement{
  const modalBody = createElement({tagName: "div", class : "modal-body"});
  const nameElement = createElement({ tagName: "span", class: "winner-name" });
  const imgElement = createElement({ tagName: "span", class: "winner-img" });
  nameElement.innerHTML = "<h2 style='color: #e60000; padding-left: 20%;'>"+ fighter.name +" is WINNER!"+"</h2>"
  imgElement.innerHTML = "<img src='" + fighter.source + "' style='width: 65%'>";
  modalBody.append(nameElement, imgElement);
  return modalBody;
}