export function fight(firstFighter, secondFighter) {
    //determines who gonna hit first
    const fighters = firstHit(firstFighter, secondFighter);
    let first = fighters[0];
    let second = fighters[1];
    while (true) {
        second.health -= getDamage(first, second);
        if (second.health <= 0) {
            return first;
        }
        first.health -= getDamage(second, first);
        if (first.health <= 0) {
            return second;
        }
    }
    // return winner
}
export function getDamage(attacker, enemy) {
    const damage = getHitPower(attacker) - getBlockPower(enemy);
    if (damage < 0) {
        return 0;
    }
    return damage;
}
export function getHitPower(fighter) {
    const dmg = fighter.attack * randomizeChance();
    return dmg;
}
export function getBlockPower(fighter) {
    const block = fighter.defense * randomizeChance();
    return block;
}
function randomizeChance() {
    return 1 + Math.random();
}
//determines who"s gonna hit first
function firstHit(firstFighter, secondFighter) {
    const pick = Math.floor(Math.random() * Math.floor(2));
    if (pick === 0) {
        return [firstFighter, secondFighter];
    }
    else {
        return [secondFighter, firstFighter];
    }
}
