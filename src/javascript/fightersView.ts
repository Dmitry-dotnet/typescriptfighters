import { createFighter } from "./fighterView";
import { showFighterDetailsModal } from "./modals/fighterDetails";
import { createElement } from "./helpers/domHelper";
import { fight } from "./fight";
import { showWinnerModal } from "./modals/winner";
import { IFighter } from "./Interfaces/IFighter";
import { getFighterDetails } from "./services/fightersService";
import { IFighterDetails } from "./Interfaces/IFighterDetails";

export function createFighters(fighters : IFighter[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: "div", className: "fighters" });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(event : HTMLInputElement, fighter : IFighter) {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId : string) : Promise<IFighterDetails> {
  return getFighterDetails(fighterId).then((result) => {
    return (result as IFighterDetails)
  } );
  // get fighter form fightersDetailsCache or use getFighterDetails function : DONE
}

function createFightersSelector() {
  const selectedFighters = new Map<string, IFighterDetails>();

  return async function selectFighterForBattle(event : Event, fighter : IFighter) {
    const fullInfo = await getFighterInfo(fighter._id);

    if ((<HTMLInputElement>event.target).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const fighters: IFighterDetails[] =[...selectedFighters.values()];
      const fisrtHp = fighters[0].health;
      const secondHp = fighters[1].health;
      const winner = fight(fighters[0], fighters[1]);
      showWinnerModal(winner);
      //restoring health
      fighters[0].health = fisrtHp;
      fighters[1].health = secondHp;

    }
  }
}
