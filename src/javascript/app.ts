import { getFighters } from "./services/fightersService"
import { createFighters } from "./fightersView";

const rootElement = document.getElementById("root");
const loadingElement = document.getElementById("loading-overlay");

export async function startApp() {
  try {
    
      (loadingElement as HTMLElement).style.visibility = "visible";
      
      const fighters = await getFighters();
      const fightersElement = createFighters(fighters);
      
      (rootElement as HTMLElement).appendChild(fightersElement);
    
  } catch (error) {
    console.warn(error);
    (rootElement as HTMLElement).innerText = "Failed to load data";
  } finally {
    (loadingElement as HTMLElement).style.visibility = "hidden";
  }
}
